<?php
/**
 * APIne Container
 *
 * @link      https://gitlab.com/apinephp/container
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/container/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpParamsInspection */
/** @noinspection ReturnTypeCanBeDeclaredInspection */
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use Apine\Container\Container;
use Apine\Container\ContainerException;
use Apine\Container\ContainerNotFoundException;
use PHPUnit\Framework\TestCase;

use function Apine\Container\createInstance;

class ContainerTest extends TestCase
{
    public function testRegisterReplacesOldValue(): void
    {
        $container = new Container();
        $container->register('containee', fn() => new StubClass());
        $this->assertInstanceOf(StubClass::class, $container->get('containee'));
        $container->register('containee', fn() => new StubClassTwo());
        $this->assertInstanceOf(StubClassTwo::class, $container->get('containee'));
    }
    
    public function testRegisterGet(): void
    {
        $container = new Container();
        $container->register(StubClass::class, fn() => new StubClass());
        
        $this->assertInstanceOf(StubClass::class, $container->get(StubClass::class));
    }
    
    public function testGetNameDifferentFromType(): void
    {
        $container = new Container();
        $container->register('containee', fn() => new StubClass());
    
        $this->assertInstanceOf(StubClass::class, $container->get('containee'));
    }
    
    public function testGetNotFound(): void
    {
        $this->expectException(ContainerNotFoundException::class);
        
        $container = new Container();
        $container->register(StubClass::class, fn() => new StubClass());
    
        $container->get(StubClassTwo::class);
    }
    
    public function testGetError(): void
    {
        $str = 'cat';
        $this->expectException(ContainerException::class);
    
        $container = new Container();
        $container->register(StubClass::class, fn() => new $str(), true);
        $container->get(StubClass::class);
    }
    
    public function testHas(): void
    {
        $container = new Container();
        $container->register(StubClass::class, fn() => new StubClass());
        
        $this->assertTrue($container->has(StubClass::class));
    }
    
    public function testRegisterCreateWithDI(): void
    {
        $container = new Container();
        $container->register('stub', createInstance(StubClass::class));
        
        $this->assertTrue($container->has('stub'));
        $this->assertInstanceOf(StubClass::class, $container->get('stub'));
    }
    
    public function testRegisterCreateWithDIWhenClassDoesNotExist(): void
    {
        $this->expectException(ContainerException::class);
        $this->expectExceptionMessageRegExp('/Class (.*?) does not exist/');
        
        $container = new Container();
        $container->register('stub', createInstance('StubClassjsdfjhk'));
        
        $this->assertTrue($container->has('stub'));
        $this->assertInstanceOf(StubClass::class, $container->get('stub'));
    }
}

class StubClass {}

class StubClassTwo{}
