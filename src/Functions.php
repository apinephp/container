<?php
/**
 * APIne Container
 *
 * @link      https://gitlab.com/apinephp/container
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/container/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Container;

use Apine\Resolver\ObjectConstructorResolver;
use Exception;
use Psr\Container\ContainerInterface;
use Closure;

/**
 * Creates an anonymous function that will create an instance
 * of the specified class using dependency injection
 *
 * @param string $class
 *          Fully qualified class name
 *
 * @return Closure
 * @throws \Apine\Container\ContainerException
 */
function createInstance(string $class): Closure
{
    if (!class_exists($class)) {
        throw new ContainerException(sprintf('Class %s does not exist', $class));
    }
    
    return static function (ContainerInterface $container) use ($class): object {
        try {
            return (new ObjectConstructorResolver($container))->resolve($class);
        } catch (Exception $e) {
            throw new ContainerException(sprintf('Error while trying to instantiate class %s', $class));
        }
    };
}