<?php
/**
 * APIne Container
 *
 * @link      https://gitlab.com/apinephp/container
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/container/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Container;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends \Exception implements ContainerExceptionInterface
{
    
}
