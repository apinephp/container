DI Container
============

A minimalistic PSR-11 DI and IoC Container

Installation
------------

Installation is made with composer

```sh
composer require apinephp/container
```

The package requires PHP 7.4 or newer.

Usage Example
-------------

```php
<?php
    
use Apine\Container\Container;

$container = new Container();
$container->register('service', function () {
    return 'A service';
});

$container->get('service'); // 'A service'
```

DI with services
----------------

Instantiate new services using the `createInstance` function. It produces a `Closure` that will create an instance of the class applying dependency resolution and injection against the services in the container.

```php
<?php
    
use Apine\Container\Container;
use function Apine\Container\createInstance;

$container = new Container();
$container->register('service', createInstance(StubClass::class));

$service = $container->get('service'); // Returns an instance of StubClass
```